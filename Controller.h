#pragma once
#include "View.h"
#include "User.h"
#include "Task.h"   
using namespace UserStuff;
using namespace TaskStuff;
 
// Controller combines Model and View
class Controller {
    public:
        Controller(const Model &model, const View &view) {
          m_model = model;
          m_view = view;       
        }
        void SetModel(const Model &model);
        void SetView(const View &view);
        void OnLoad();

        ~Controller(){
            std::cout << "Destroyed Controller" << std::endl;
        }

     private:
        Model m_model;
        View m_view;
};
