#include "Log.h"
#include <iostream>

void Log::Set_Level(int level){
	m_LogLevel = level;
}

void Log::Error(const char* message) {
	if (m_LogLevel >= LogLevelError) 
		std::cout << "[ERROR]: " << message << std::endl;
}

void Log::Info(const char* message){
	if (m_LogLevel >= LogLevelInfo) 
		std::cout << "[INFO]: " << message << std::endl;
		}

void Log::Warn(const char* message){
	if(m_LogLevel >= LogLevelWarning) 
		std::cout << "[WARNING]: " << message << std::endl;
}

void Log::Print(const char* message) {
	std::cout << message << std::endl;
}
