#pragma once

class Log{

	public:
		const int LogLevelError = 0;
		const int LogLevelWarning = 1;
		const int LogLevelInfo = 2; 
	private:	
        int m_LogLevel;

	public:
        Log(){}
		void Set_Level(int level);
		void Error(const char* message);
		void Info(const char* message);
		void Warn(const char* message);
        void Print(const char* message);
};
