#include <iostream>
#include "Controller.h"

void Controller::OnLoad() {
    m_view.Render();
}

void Controller::SetView(const View &view) {
    m_view = view;
}

void Controller::SetModel(const Model &model) {
    m_model = model;
}

