#pragma once
#include <iostream>                  
#include "User.h"
#include "Task.h"   
using namespace UserStuff;
using namespace TaskStuff;

struct Model {
    User user;
    std::shared_ptr<TaskStuff::Task> t_ptr;
};

class View {
    public:
        View() {}
        View(Model model) {
            m_model = model;
        }
    
        ~View() {
            std::cout << "Destroyed View" << std::endl;
        }
        void Render();
    private:
        Model m_model;
};